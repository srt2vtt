srt2vtt
=======

Convert SRT formatted subtitles to WebVTT format for use with the
HTML5 `<track>` tag.

Usage
-----

```
$ srt2vtt --help
Usage: srt2vtt [OPTIONS]
Convert SubRip formatted subtitles to WebVTT format.

  -h, --help             display this help and exit
  -v, --version          display version and exit
  -i, --input=FILE-NAME  read input from FILE-NAME
  -o, --output=FILE-NAME write output to FILE-NAME
```

If `--input` or `--output` is ommitted, read from stdin or stdout,
respectively.

Installation
------------

```
./bootstrap
./configure
make
make install
```

Alternatively, just copy `srt2vtt` yourself to wherever you would like
it.  Or, call it from your own git checkout.

Requirements
------------

* GNU Guile >= 2.0.5

License
-------

GNU GPLv3+
