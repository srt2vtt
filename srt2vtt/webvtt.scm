;;; srt2vtt --- SRT to WebVTT converter
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;;
;;; srt2vtt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; srt2vtt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with srt2vtt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (srt2vtt webvtt)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srt2vtt)
  #:export (write-webvtt
            write-webvtts))

(define (write-time time port)
  "Write TIME as a WebVTT formatted timestamp to PORT."
  (match time
   ((h m s ms)
    (format port "~2,'0d:~2,'0d:~2,'0d.~3,'0d" h m s ms))))

(define (write-webvtt subtitle port)
  "Write SUBTITLE as a WebVTT formatted subtitle to PORT."
  (format port "~a~%" (subtitle-id subtitle))
  (write-time (subtitle-start subtitle) port)
  (display " --> " port)
  (write-time (subtitle-end subtitle) port)
  (newline port)
  (format port "~a~%" (string-join (subtitle-text subtitle) "\n"))
  (newline port))

(define (write-webvtts subtitles port)
  "Write all SUBTITLES as WebVTT formatted subtitles to PORT."
  (format port "WEBVTT~%~%")
  (for-each (cut write-webvtt <> port) subtitles))
