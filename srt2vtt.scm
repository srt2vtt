;;; srt2vtt --- SRT to WebVTT converter
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;;
;;; srt2vtt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; srt2vtt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with srt2vtt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (srt2vtt)
  #:use-module (srfi srfi-9)
  #:export (make-subtitle
            subtitle?
            subtitle-id
            subtitle-start
            subtitle-end
            subtitle-text))

(define-record-type <subtitle>
  (make-subtitle id start end text)
  subtitle?
  (id subtitle-id)
  (start subtitle-start)
  (end subtitle-end)
  (text subtitle-text))
