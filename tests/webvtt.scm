;;; srt2vtt --- SRT to WebVTT converter
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;;
;;; srt2vtt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; srt2vtt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with srt2vtt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests webvtt)
  #:use-module (srfi srfi-64)
  #:use-module (tests utils)
  #:use-module (srt2vtt)
  #:use-module (srt2vtt webvtt))

(test-begin "webvtt")

(test-equal "write-webvtt"
  "1
01:02:03.004 --> 05:06:07.008
foo

"
  (call-with-output-string
   (lambda (port)
     (let ((sub (make-subtitle 1
                               '(1 2 3 4)
                               '(5 6 7 8)
                               '("foo"))))
       (write-webvtt sub port)))))

(test-equal "write-webvtts"
  "WEBVTT

1
01:02:03.004 --> 05:06:07.008
foo

2
09:10:11.012 --> 13:14:15.016
bar
baz

"
  (pk 'subs(call-with-output-string
    (lambda (port)
      (let ((subs (list
                   (make-subtitle 1
                                  '(1 2 3 4)
                                  '(5 6 7 8)
                                  '("foo"))
                   (make-subtitle 2
                                  '(9 10 11 12)
                                  '(13 14 15 16)
                                  '("bar" "baz")))))
        (write-webvtts subs port))))))

(test-end* "webvtt")
