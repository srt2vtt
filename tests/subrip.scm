;;; srt2vtt --- SRT to WebVTT converter
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;;
;;; srt2vtt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; srt2vtt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with srt2vtt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests subrip)
  #:use-module (srfi srfi-64)
  #:use-module (tests utils)
  #:use-module (srt2vtt)
  #:use-module (srt2vtt subrip))

(test-begin "subrip")

(test-group "read-subrip"
  (define (read-subrip-string str)
    (call-with-input-string str read-subrip))

  (test-equal (make-subtitle 1
                             '(1 2 3 4)
                             '(5 6 7 8)
                             '("foo" "bar" "baz"))
    (read-subrip-string
     "1
01:02:03,004 --> 05:06:07,008
foo
bar
baz
"))

  ;; Blank subtitle text
  (test-equal (make-subtitle 1
                             '(1 2 3 4)
                             '(5 6 7 8)
                             '(""))
    (read-subrip-string
     "1
01:02:03,004 --> 05:06:07,008

")))

(test-equal "read-subrips"
  (list (make-subtitle 1
                       '(1 2 3 4)
                       '(5 6 7 8)
                       '("foo"))
        (make-subtitle 2
                       '(9 10 11 12)
                       '(13 14 15 16)
                       '("bar" "baz")))
  (call-with-input-string "1
01:02:03,004 --> 05:06:07,008
foo

2
09:10:11,012 --> 13:14:15,016
bar
baz
"
    read-subrips))

(test-end* "subrip")
